object ClientModule1: TClientModule1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    ConnectionName = 'DataSnapCONNECTION'
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=DataSnap'
      'HostName=localhost'
      'port=211')
    Connected = True
    Left = 168
    Top = 56
    UniqueId = '{2C81D9C7-B42C-40E0-A745-6F6E48C2DAA5}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 168
    Top = 112
  end
  object cdsCategory: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCategory'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsCategoryAfterPost
    Left = 96
    Top = 184
    object cdsCategoryID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 200
    end
  end
  object cdsService: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspService'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsServiceAfterPost
    Left = 192
    Top = 184
    object cdsServiceID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsServiceCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Required = True
    end
    object cdsServiceNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object cdsServiceDURATION: TIntegerField
      FieldName = 'DURATION'
    end
    object cdsServicePRICE: TSingleField
      FieldName = 'PRICE'
    end
  end
end
