//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ServerMethodsUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TServerMethods1::TServerMethods1(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::ReverseString(System::UnicodeString value)
{
	return ::ReverseString(value);
}
//----------------------------------------------------------------------------
void TServerMethods1::NewItem(int category_id, UnicodeString name, int duration, int price)
{
	Service_insProc->Params->ParamByName("CATEGORY_ID")->Value = category_id;
	Service_insProc->Params->ParamByName("NAME")->Value = name;
	Service_insProc->Params->ParamByName("DURATION")->Value = duration;
	Service_insProc->Params->ParamByName("PRICE")->Value = price;
	Service_insProc->ExecProc();
}
void __fastcall TServerMethods1::FirstConnectionBeforeConnect(TObject *Sender)
{
	FirstConnection->Params->Values["DataBase"] = "..\\..\\FIRST.FDB";
}
//---------------------------------------------------------------------------

