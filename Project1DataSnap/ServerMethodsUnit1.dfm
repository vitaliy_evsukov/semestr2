object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 454
  Width = 525
  object FirstConnection: TSQLConnection
    ConnectionName = 'first'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      
        'Database=C:\Users\VD\Documents\Embarcadero\Studio\Project\FIRST.' +
        'FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    BeforeConnect = FirstConnectionBeforeConnect
    Left = 111
    Top = 26
  end
  object CategoryTable: TSQLDataSet
    CommandText = 'CATEGORY'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = FirstConnection
    Left = 74
    Top = 97
  end
  object ServiceTable: TSQLDataSet
    CommandText = 'NEW_TABLE'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = FirstConnection
    Left = 166
    Top = 99
  end
  object dspCategory: TDataSetProvider
    DataSet = CategoryTable
    Left = 72
    Top = 176
  end
  object dspService: TDataSetProvider
    DataSet = ServiceTable
    Left = 160
    Top = 176
  end
  object Category_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'ID'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 50
        Name = 'NAME'
        ParamType = ptInput
      end>
    SQLConnection = FirstConnection
    StoredProcName = 'CATEGORY_INS'
    Left = 66
    Top = 260
  end
  object Service_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'CATEGORY_ID'
        ParamType = ptInput
      end
      item
        DataType = ftWideString
        Precision = 60
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'DURATION'
        ParamType = ptInput
      end
      item
        DataType = ftSingle
        Precision = 4
        Name = 'PRICE'
        ParamType = ptInput
      end>
    SQLConnection = FirstConnection
    StoredProcName = 'NEW_TABLE_INS'
    Left = 169
    Top = 258
  end
end
