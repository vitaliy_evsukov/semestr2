//---------------------------------------------------------------------------

#ifndef ServerMethodsUnit1H
#define ServerMethodsUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TServerMethods1 : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *FirstConnection;
	TSQLDataSet *CategoryTable;
	TSQLDataSet *ServiceTable;
	TDataSetProvider *dspCategory;
	TDataSetProvider *dspService;
	TSQLStoredProc *Category_insProc;
	TSQLStoredProc *Service_insProc;
	void __fastcall FirstConnectionBeforeConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TServerMethods1(TComponent* Owner); 
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
void NewItem(int category_id, UnicodeString name, int duration, int price);
};
//---------------------------------------------------------------------------
extern PACKAGE TServerMethods1 *ServerMethods1;
//---------------------------------------------------------------------------
#endif

