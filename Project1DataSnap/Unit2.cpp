//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
#include "ClientModuleUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
	TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::tbAddClick(TObject *Sender)
{
	TabControl1->ActiveTab = tiAdd;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::tbEditClick(TObject *Sender)
{
	TabControl1->ActiveTab = tiEdit;
}
//---------------------------------------------------------------------------


void __fastcall TForm2::bEditFalseClick(TObject *Sender)
{
	TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::tbDelClick(TObject *Sender)
{
ClientModule1->cdsService->Delete();
ClientModule1->cdsService->ApplyUpdates(-1);
ClientModule1->cdsService->Refresh();
TabControl1->ActiveTab = tiMain;

}
//---------------------------------------------------------------------------


void __fastcall TForm2::ListView1Change(TObject *Sender)
{
	TabControl1->ActiveTab = tiEdit;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::bEditTrueClick(TObject *Sender)
{
	ClientModule1->cdsService->Edit();
	ClientModule1->cdsService->FieldByName("ID")->AsInteger = ClientModule1->cdsServiceID->AsInteger;
	ClientModule1->cdsService->FieldByName("NAME")->AsString = eName1->Text;
	ClientModule1->cdsService->FieldByName("DURATION")->AsString = eDuration1->Text;
	ClientModule1->cdsService->FieldByName("PRICE")->AsString = ePrice1->Text;
	ClientModule1->cdsService->FieldByName("CATEGORY_ID")->AsInteger = ClientModule1->cdsCategoryID->AsInteger;
	ClientModule1->cdsService->Post();
	ClientModule1->cdsService->Refresh();
	TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::bAddTrueClick(TObject *Sender)
{
	ClientModule1->ServerMethods1Client->NewItem(
	ClientModule1->cdsCategoryID->Value,
	eName->Text,
	eTime->Text.ToInt(),
	ePrice->Text.ToInt()

	);
	ClientModule1->cdsService->Refresh();
	TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

