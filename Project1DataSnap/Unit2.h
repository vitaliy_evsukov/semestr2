//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <System.Rtti.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <System.Bindings.Outputs.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TBindSourceDB *BindSourceDB2;
	TTabControl *TabControl1;
	TTabItem *tiMain;
	TTabItem *tiAdd;
	TButton *tbAdd;
	TButton *tbEdit;
	TButton *tbDel;
	TTabItem *tiEdit;
	TLabel *lAdd;
	TEdit *eName;
	TLabel *tName;
	TComboBox *cbGroup;
	TLabel *lGroup;
	TLabel *lTime;
	TEdit *eTime;
	TLabel *lPrice;
	TEdit *ePrice;
	TComboBox *cbCategory1;
	TEdit *ePrice1;
	TEdit *eName1;
	TEdit *eDuration1;
	TLabel *lEdit;
	TLabel *lCategory1;
	TLabel *lPrice1;
	TLabel *lDuration1;
	TLabel *lName1;
	TButton *bEditTrue;
	TButton *bEditFalse;
	TButton *bAddFalse;
	TButton *bAddTrue;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkControlToField *LinkControlToField3;
	TLinkListControlToField *LinkListControlToField1;
	TLinkFillControlToField *LinkFillControlToField1;
	TListView *ListView1;
	TLinkListControlToField *LinkListControlToField2;
	TLinkListControlToField *LinkListControlToField3;
	void __fastcall tbAddClick(TObject *Sender);
	void __fastcall tbEditClick(TObject *Sender);
	void __fastcall bEditFalseClick(TObject *Sender);
	void __fastcall tbDelClick(TObject *Sender);
	void __fastcall ListView1Change(TObject *Sender);
	void __fastcall bEditTrueClick(TObject *Sender);
	void __fastcall bAddTrueClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
