//
// Created by the DataSnap proxy generator.
// 17.06.2017 13:05:23
// 

#include "uProxy.h"

System::UnicodeString __fastcall TServerMethods1Client::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TServerMethods1.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TServerMethods1Client::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TServerMethods1.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TServerMethods1Client::NewItem(int category_id, System::UnicodeString name, int duration, int price)
{
  if (FNewItemCommand == NULL)
  {
    FNewItemCommand = FDBXConnection->CreateCommand();
    FNewItemCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FNewItemCommand->Text = "TServerMethods1.NewItem";
    FNewItemCommand->Prepare();
  }
  FNewItemCommand->Parameters->Parameter[0]->Value->SetInt32(category_id);
  FNewItemCommand->Parameters->Parameter[1]->Value->SetWideString(name);
  FNewItemCommand->Parameters->Parameter[2]->Value->SetInt32(duration);
  FNewItemCommand->Parameters->Parameter[3]->Value->SetInt32(price);
  FNewItemCommand->ExecuteUpdate();
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TServerMethods1Client::~TServerMethods1Client()
{
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FNewItemCommand;
}

