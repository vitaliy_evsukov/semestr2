// 
// Created by the DataSnap proxy generator.
// 19.06.2017 17:28:31
// 

#include "ClientClassesUnit1.h"

void __fastcall TServerMethods1Client::DbConnectionBeforeConnect(TJSONObject* Sender)
{
  if (FDbConnectionBeforeConnectCommand == NULL)
  {
    FDbConnectionBeforeConnectCommand = FDBXConnection->CreateCommand();
    FDbConnectionBeforeConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FDbConnectionBeforeConnectCommand->Text = "TServerMethods1.DbConnectionBeforeConnect";
    FDbConnectionBeforeConnectCommand->Prepare();
  }
  FDbConnectionBeforeConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FDbConnectionBeforeConnectCommand->ExecuteUpdate();
}

System::UnicodeString __fastcall TServerMethods1Client::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TServerMethods1.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TServerMethods1Client::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TServerMethods1.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TServerMethods1Client::NewItem(System::UnicodeString name, int tanswer, int fanswer)
{
  if (FNewItemCommand == NULL)
  {
    FNewItemCommand = FDBXConnection->CreateCommand();
    FNewItemCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FNewItemCommand->Text = "TServerMethods1.NewItem";
    FNewItemCommand->Prepare();
  }
  FNewItemCommand->Parameters->Parameter[0]->Value->SetWideString(name);
  FNewItemCommand->Parameters->Parameter[1]->Value->SetInt32(tanswer);
  FNewItemCommand->Parameters->Parameter[2]->Value->SetInt32(fanswer);
  FNewItemCommand->ExecuteUpdate();
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TServerMethods1Client::~TServerMethods1Client()
{
  delete FDbConnectionBeforeConnectCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FNewItemCommand;
}

