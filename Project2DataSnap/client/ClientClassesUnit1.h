#ifndef ClientClassesUnit1H
#define ClientClassesUnit1H

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TServerMethods1Client : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FDbConnectionBeforeConnectCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
    TDBXCommand *FNewItemCommand;
  public:
    __fastcall TServerMethods1Client(TDBXConnection *ADBXConnection);
    __fastcall TServerMethods1Client(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TServerMethods1Client();
    void __fastcall DbConnectionBeforeConnect(TJSONObject* Sender);
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
    void __fastcall NewItem(System::UnicodeString name, int tanswer, int fanswer);
  };

#endif
