//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "ClientModuleUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
__fastcall TClientModule1::TClientModule1(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall TClientModule1::~TClientModule1()
{
	delete FServerMethods1Client;
}

TServerMethods1Client* TClientModule1::GetServerMethods1Client(void)
{
	if (FServerMethods1Client == NULL)
	{
		SQLConnection1->Open();
		FServerMethods1Client = new TServerMethods1Client(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FServerMethods1Client;
};

void __fastcall TClientModule1::cdsAnswerAfterPost(TDataSet *DataSet)
{
	cdsAnswer->ApplyUpdates(-1);
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::DataModuleCreate(TObject *Sender)
{
    cdsAnswer->Open();
}
//---------------------------------------------------------------------------
