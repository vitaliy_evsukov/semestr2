object ClientModule1: TClientModule1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    ConnectionName = 'DataSnapCONNECTION'
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'DriverName=DataSnap'
      'HostName=localhost'
      'port=211'
      'Filters={}')
    Connected = True
    Left = 168
    Top = 48
    UniqueId = '{030E1841-7A9B-4FA2-AEFC-99078EAC7B18}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 176
    Top = 112
  end
  object cdsAnswer: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspAnswer'
    RemoteServer = DSProviderConnection1
    AfterPost = cdsAnswerAfterPost
    Left = 168
    Top = 200
    object cdsAnswerID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsAnswerNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 200
    end
    object cdsAnswerTANSWER: TIntegerField
      FieldName = 'TANSWER'
      Required = True
    end
    object cdsAnswerFANSWER: TIntegerField
      FieldName = 'FANSWER'
      Required = True
    end
  end
end
