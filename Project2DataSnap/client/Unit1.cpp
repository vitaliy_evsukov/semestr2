//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "ClientModuleUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
int a;
int b;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
    TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::bStartClick(TObject *Sender)
{
	TabControl1->ActiveTab = tiAnswer1;
	a=0;
	b=2;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::bBackClick(TObject *Sender)
{
    ClientModule1->ServerMethods1Client->NewItem(
		eName->Text,
		a,
		b

	);
	ClientModule1->cdsAnswer->Refresh();
    eName->Text = "";
	TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bAns12Click(TObject *Sender)
{

	a=a+1;
	b=b-1;
	TabControl1->ActiveTab = tiAnswer2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bAns11Click(TObject *Sender)
{
    TabControl1->ActiveTab = tiAnswer2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bAns21Click(TObject *Sender)
{
	TabControl1->ActiveTab = tiEnd;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bAns23Click(TObject *Sender)
{
	a = a+1;
	b = b-1;
	TabControl1->ActiveTab = tiEnd;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::bStatiClick(TObject *Sender)
{
    TabControl1->ActiveTab = tiStatistic;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bBack1Click(TObject *Sender)
{
    TabControl1->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::bAboutClick(TObject *Sender)
{
	ShowMessage("���� � �������������� DataSnap");
}
//---------------------------------------------------------------------------

