//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <System.Rtti.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <System.Bindings.Outputs.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *tiMain;
	TTabItem *tiAnswer1;
	TTabItem *tiAnswer2;
	TTabItem *tiEnd;
	TTabItem *tiStatistic;
	TLabel *lName;
	TEdit *eName;
	TButton *bStart;
	TButton *bAbout;
	TLabel *Label1;
	TButton *bAns11;
	TButton *bAns12;
	TButton *bAns13;
	TLabel *Label2;
	TButton *bAns21;
	TButton *bAns22;
	TButton *bAns23;
	TLabel *Label3;
	TButton *bStati;
	TButton *bBack;
	TGrid *Grid1;
	TButton *bBack1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkGridToDataSource *LinkGridToDataSourceBindSourceDB1;
	void __fastcall bStartClick(TObject *Sender);
	void __fastcall bBackClick(TObject *Sender);
	void __fastcall bAns12Click(TObject *Sender);
	void __fastcall bAns11Click(TObject *Sender);
	void __fastcall bAns21Click(TObject *Sender);
	void __fastcall bAns23Click(TObject *Sender);
	void __fastcall bStatiClick(TObject *Sender);
	void __fastcall bBack1Click(TObject *Sender);
	void __fastcall bAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
