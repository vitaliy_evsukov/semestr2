//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ServerMethodsUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TServerMethods1::TServerMethods1(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------


void __fastcall TServerMethods1::DbConnectionBeforeConnect(TObject *Sender)
{
	DbConnection->Params->Values["DataBase"] = "..\\..\\..\\DB.FDB";
}
//---------------------------------------------------------------------------
void TServerMethods1::NewItem(UnicodeString name, int tanswer, int fanswer)
{

	Answer_insProc->Params->ParamByName("NAME")->Value = name;
	Answer_insProc->Params->ParamByName("TANSWER")->Value = tanswer;
	Answer_insProc->Params->ParamByName("FANSWER")->Value = fanswer;
	Answer_insProc->ExecProc();
}