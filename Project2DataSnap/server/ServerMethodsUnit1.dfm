object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 306
  Width = 458
  object DbConnection: TSQLConnection
    ConnectionName = 'db'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\Users\VD\Desktop\project2\DB.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    BeforeConnect = DbConnectionBeforeConnect
    Left = 134
    Top = 38
  end
  object AnswerTable: TSQLDataSet
    CommandText = 'ANSWER'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = DbConnection
    Left = 133
    Top = 105
  end
  object dspAnswer: TDataSetProvider
    DataSet = AnswerTable
    Left = 136
    Top = 168
  end
  object Answer_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Precision = 50
        Name = 'NAME'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'TANSWER'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'FANSWER'
        ParamType = ptInput
      end>
    SQLConnection = DbConnection
    StoredProcName = 'ANSWER_INS'
    Left = 134
    Top = 238
  end
end
