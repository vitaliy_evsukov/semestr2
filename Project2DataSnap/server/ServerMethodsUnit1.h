//---------------------------------------------------------------------------

#ifndef ServerMethodsUnit1H
#define ServerMethodsUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TServerMethods1 : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *DbConnection;
	TSQLDataSet *AnswerTable;
	TDataSetProvider *dspAnswer;
	TSQLStoredProc *Answer_insProc;
	void __fastcall DbConnectionBeforeConnect(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TServerMethods1(TComponent* Owner); 
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
void NewItem(UnicodeString name, int tanswer, int fanswer);
};
//---------------------------------------------------------------------------
extern PACKAGE TServerMethods1 *ServerMethods1;
//---------------------------------------------------------------------------
#endif

